# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/examples/libs/gl3w/GL/gl3w.c" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/examples/libs/gl3w/GL/gl3w.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GLFW_DLL"
  "IMGUI_IMPL_OPENGL_LOADER_GL3W"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../src/joyui/src/imcore"
  "../src/joyui/src/imcore/imgui"
  "../src/joyui/src/imcore/imgui/backends"
  "../src/joyui/src/imcore/imgui/examples/libs/gl3w"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/backends/imgui_impl_glfw.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/backends/imgui_impl_glfw.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/backends/imgui_impl_opengl3.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/backends/imgui_impl_opengl3.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/imgui.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/imgui.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/imgui_demo.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/imgui_demo.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/imgui_draw.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/imgui_draw.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/imgui_tables.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/imgui_tables.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/imgui/imgui_widgets.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/imgui/imgui_widgets.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/implot/implot.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/implot/implot.cpp.o"
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/implot/implot_items.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/implot/implot_items.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLFW_DLL"
  "IMGUI_IMPL_OPENGL_LOADER_GL3W"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/joyui/src/imcore"
  "../src/joyui/src/imcore/imgui"
  "../src/joyui/src/imcore/imgui/backends"
  "../src/joyui/src/imcore/imgui/examples/libs/gl3w"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
