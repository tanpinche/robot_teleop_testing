# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/imcore/sample/imgui_sample.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/sample/CMakeFiles/imgui_sample.dir/imgui_sample.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLFW_DLL"
  "IMGUI_IMPL_OPENGL_LOADER_GL3W"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/joyui/src/imcore"
  "../src/joyui/src/imcore/imgui"
  "../src/joyui/src/imcore/imgui/backends"
  "../src/joyui/src/imcore/imgui/examples/libs/gl3w"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
