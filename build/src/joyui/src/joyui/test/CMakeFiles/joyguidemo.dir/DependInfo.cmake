# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/common/Desktop/robot_teleop_testing/src/joyui/src/joyui/test/joygui_demo.cpp" "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/joyui/test/CMakeFiles/joyguidemo.dir/joygui_demo.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASIO_STANDALONE"
  "GLFW_DLL"
  "IMGUI_IMPL_OPENGL_LOADER_GL3W"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src/joyui/src/joyui/include"
  "../src/joyui/src/imcore"
  "../src/joyui/src/imcore/imgui"
  "../src/joyui/src/imcore/imgui/backends"
  "../src/joyui/src/imcore/imgui/examples/libs/gl3w"
  "../src/wrp_sdk/include"
  "../src/wrp_sdk/include/asio/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/joyui/CMakeFiles/joyui.dir/DependInfo.cmake"
  "/home/common/Desktop/robot_teleop_testing/build/src/joyui/src/imcore/CMakeFiles/imcore.dir/DependInfo.cmake"
  "/home/common/Desktop/robot_teleop_testing/build/src/wrp_sdk/CMakeFiles/wrp_sdk.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
