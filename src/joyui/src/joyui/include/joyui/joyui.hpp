#include "implot/implot.h"
#include <string.h>
#include <thread>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "implot/implot.h"

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include "wrp_sdk/mobile_base/agilex/scout_base.hpp"
#include "wrp_sdk/mobile_base/agilex/tracer_base.hpp"
#include "wrp_sdk/periph_device/joystick.hpp"

/*using namespace rdu;
struct ImDraw : public ImCanvas {
  void Draw() override {
    // do nothing
  }
};*/

namespace westonrobot {
class JoyGui {
 public:
  JoyGui(uint32_t width = 640, uint32_t height = 480,
         std::string title = "JoyGui");

  ~JoyGui();

  // virtual functions
  virtual double CalculateLinearVeloctiy();
  virtual double CalculateAngularVeloctiy();
  virtual void AdditionalFunctions();

  void Draw();
  bool send_inputs = false;

  westonrobot::Joystick joyctl;

 private:
  // variables for window size settings
  bool initialized_ = false;
  uint32_t width_ = 500, height_ = 700;
  GLFWwindow *window_;
  ImVec4 background_color_;
  bool resize_after_connect_ = false;

  // variables for widget use
  int window_width_, window_height_;
  int current_height_ = 100, current_width_ = 100;
  ImVec2 cursor_pos_;

 
  // robot types
  westonrobot::ScoutBase scout;
  westonrobot::TracerBase tracer;
   // variables for drop down lists
  int linked_robot_ = 0;
  int selectedjs_;
  int selectedcan_;
  int selectedrobot_;
  std::string device_name_;

  const char *robot_type_[2] = {"Scout", "Tracer"};
  std::vector <std::string> can_port_ = {"vcan0", "can0"};
};

}  // namespace westonrobot
