#include "joyui/joyui.hpp"

namespace westonrobot {
namespace {
static void glfw_error_callback(int error, const char* description) {
  fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}
}  // namespace

JoyGui::JoyGui(uint32_t width, uint32_t height, std::string title) {
  selectedjs_ = 0;
  selectedcan_ = 0;
  selectedrobot_ = 0;

  // Setup window
  glfwSetErrorCallback(glfw_error_callback);
  if (!glfwInit()) return;

  // Decide GL+GLSL versions
  const char* glsl_version = "#version 130";
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+
  // only glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // 3.0+ only

  // Create window with graphics context
  window_ = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
  if (window_ == NULL) return;
  glfwMakeContextCurrent(window_);
  glfwSwapInterval(1);  // Enable vsync

  // Initialize OpenGL loader
  if (gl3wInit() != 0) return;

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImPlot::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  (void)io;
  // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
  //   io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;
  // Enable Gamepad Controls

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  // ImGui::StyleColorsClassic();

  // Setup Platform/Renderer backends
  ImGui_ImplGlfw_InitForOpenGL(window_, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  // Set default background color to be white
  background_color_ = ImVec4(1.0f, 1.0f, 1.0f, 1.00f);

  // Everything is okay if reached here
  initialized_ = true;
}

void JoyGui::Draw() {
  const char* js[joyctl.list_of_joystick_.size()];
  for (int x = 0; x < joyctl.list_of_joystick_.size(); x++) {
    js[x] = joyctl.list_of_joystick_[x].c_str();
  }

  while (!glfwWindowShouldClose(window_)) {
    // Poll and handle events (inputs, window resize, etc.)
    glfwPollEvents();

    if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape))) break;

    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    // Begining of ImGui drawing

    ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(0.2, 0.2, 0.35, 1));
    // set size of imgui window to match glfwWindow
    glfwGetWindowSize(window_, &window_width_, &window_height_);
    ImGui::SetWindowSize(ImVec2(window_width_, window_height_));
    ImGui::SetWindowPos(ImVec2(0, 0));

    // resize window to fit all widgets after connecting to a joystick
    if (resize_after_connect_) {
      glfwSetWindowSize(window_, current_width_ + 150, current_height_ + 150);
      resize_after_connect_ = false;
    }
    // button to manually resize window to fit all existing widgets
    if (ImGui::Button("Fit Window")) {
      glfwSetWindowSize(window_, current_width_ + 150, current_height_ + 150);
    }
    // interface for connecting to joystick
    ImGui::Text("Joystick Settings");
    ImGui::Text("Connect to:");
    ImGui::SameLine();
    ImGui::PushItemWidth(50);
    ImGui::Combo("", &selectedjs_, js, joyctl.list_of_joystick_.size());
    ImGui::SameLine();
    if (ImGui::Button("connect")) {
      joyctl.Connect(js[selectedjs_]);
      resize_after_connect_ = true;
    }

    // display joystick info
    auto joystick_name = joyctl.GetCurrentDeviceName();
    ImGui::SameLine();
    ImGui::Text("Connected to");
    ImGui::Text(device_name_.c_str());
    ImGui::Text("Has %d Axes", joyctl.GetNumberOfAxis());
    ImGui::SameLine();
    ImGui::Text("and %d buttons", joyctl.GetNumberOfButton());
    ImGui::Text("Thread Status:");
    ImGui::SameLine();
    // display ReadThread status
    if (joyctl.IsThreadActive())
      ImGui::TextColored(ImVec4(0, 1, 0, 1), "ACTIVE");
    else
      ImGui::TextColored(ImVec4(1, 0, 0, 1), "INACTIVE");
    if (ImGui::Button("Start Reading Thread")) joyctl.StartReadThread();
    ImGui::SameLine();
    if (ImGui::Button("Stop Reading Thread")) joyctl.StopRead();

    // interface for connecting to CAN
    const char* can_port[can_port_.size()];
    for (int x = 0; x < can_port_.size(); x++) {
      can_port[x] = can_port_[x].c_str();
    }
    ImGui::Text("CAN Settings");
    ImGui::PushItemWidth(65);
    ImGui::Combo(" ", &selectedcan_, can_port, IM_ARRAYSIZE(can_port));
    ImGui::SameLine();
    if (ImGui::Button("Connect to CAN")) {
      device_name_ = can_port_[selectedcan_];
    }
    ImGui::SameLine();
    char user_input[20]="";
    ImGui::InputText("", user_input, IM_ARRAYSIZE(user_input));
    ImGui::SameLine();
    if (ImGui::Button("add port")) {
      can_port_.push_back(user_input);
      
    }
    
    ImGui::Text("Connected to");
    ImGui::SameLine();
    ImGui::Text(device_name_.c_str());
    ImGui::Text(" ");

    // Interface for selecting robot type
    ImGui::Text("Robot Settings");
    ImGui::Combo("  ", &selectedrobot_, robot_type_, IM_ARRAYSIZE(robot_type_));

    ImGui::SameLine();
    if (ImGui::Button("Select Robot")) {
      switch (selectedrobot_) {
        case 0:
          linked_robot_ = selectedrobot_;
          scout.Connect(device_name_);
          break;

        case 1:
          linked_robot_ = selectedrobot_;
          // TracerBase tracer;
          tracer.Connect(device_name_);
          break;
      }
    }
    ImGui::Text("Connected to");
    ImGui::SameLine();
    ImGui::Text(robot_type_[linked_robot_]);
    ImGui::Text(" ");

    // PublishThread status and control
    ImGui::Text("Robot Type");
    ImGui::Text("Sending Inputs:");
    ImGui::SameLine();
    if (send_inputs)
      ImGui::TextColored(ImVec4(0, 1, 0, 1), "ACTIVE");
    else
      ImGui::TextColored(ImVec4(1, 0, 0, 1), "INACTIVE");
    if (ImGui::Button("Start Sending Joystick Data")) send_inputs = true;
    ImGui::SameLine();
    if (ImGui::Button("Stop Sending Joystick Data")) send_inputs = false;

    // diplaying values of joystick axes and buttons
    ImGui::Columns(2, "Values");
    ImGui::SetColumnWidth(0, 420);
    for (int i = 0; i < joyctl.GetNumberOfAxis(); i++) {
      ImGui::Text("Axis %d", i);
      cursor_pos_ = ImGui::GetCursorScreenPos();

      // creating of the progress bar to display negative axis values
      ImGui::GetWindowDrawList()->AddRectFilled(
          cursor_pos_, ImVec2(cursor_pos_.x + 199, cursor_pos_.y + 19),
          ImGui::GetColorU32(ImGuiCol_PlotHistogram));
      ImGui::GetWindowDrawList()->AddRectFilled(
          cursor_pos_,
          ImVec2(cursor_pos_.x + 199 - joyctl.GetAxisValue(i) * -199,
                 cursor_pos_.y + 19),
          ImGui::GetColorU32(ImGuiCol_FrameBg));

      ImGui::SetCursorPos(
          ImVec2(cursor_pos_.x + 176 - joyctl.GetAxisValue(i) * -199,
                 cursor_pos_.y + 3));
      if (joyctl.GetAxisValue(i) < 0 && joyctl.GetAxisValue(i) > -0.85)
        ImGui::Text("%.0f%%", -100 * joyctl.GetAxisValue(i));
      else if (joyctl.GetAxisValue(i) < 0) {
        ImGui::SetCursorPos(ImVec2(cursor_pos_.x + 2, cursor_pos_.y + 2));
        ImGui::Text("%.0f%%", -100 * joyctl.GetAxisValue(i));
      }

      cursor_pos_.x += 200;
      ImGui::SetCursorPos(cursor_pos_);
      ImGui::ProgressBar(joyctl.GetAxisValue(i), ImVec2(199.0f, 0.0f));
    }
    ImGui::NextColumn();
    for (int i = 0; i < joyctl.GetNumberOfButton(); i++) {
      if (joyctl.GetButtonValue(i) == 1) {
        ImGui::Text("Button %d:", i);
        ImGui::SameLine();
        ImGui::TextColored(ImVec4(0, 1, 0, 1), "ON");
      } else
        ImGui::Text("Button %d: OFF", i);

      cursor_pos_ = ImGui::GetCursorPos();
      if (current_width_ < cursor_pos_.x) current_width_ = cursor_pos_.x;
      if (current_height_ < cursor_pos_.y) current_height_ = cursor_pos_.y;
    }

    AdditionalFunctions();
    // send commands to robot
    if (joyctl.IsThreadActive() && send_inputs) {
      switch (linked_robot_) {
        case 0:
          scout.SetMotionCommand(CalculateLinearVeloctiy(),
                                 CalculateAngularVeloctiy());
          break;

        case 1:
          tracer.SetMotionCommand(CalculateLinearVeloctiy(),
                                  CalculateAngularVeloctiy());
          break;
      }
    }

    ImGui::Columns();

    // Rendering
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(window_, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(background_color_.x, background_color_.y, background_color_.z,
                 background_color_.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(window_);
  }
};

JoyGui::~JoyGui() {
  // Cleanup
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImPlot::DestroyContext();
  ImGui::DestroyContext();

  glfwDestroyWindow(window_);
  glfwTerminate();
}

double JoyGui::CalculateLinearVeloctiy() { return 0; }

double JoyGui::CalculateAngularVeloctiy() { return 0; };

void JoyGui::AdditionalFunctions() { int x = 1; }
}  // namespace westonrobot
